import os

inp = open("in.txt")
out = None

try:
    out = open("out.txt", "xt")
except:
    os.remove("out.txt")
    out = open("out.txt", "xt")
    

for line in inp.readlines():
    line = line.strip()
    if len(line) == 0:
        out.writelines((["none,\n"]))
    else:
        out.writelines((["[" + line + "],\n"]))
    
    
inp.close()
out.close()