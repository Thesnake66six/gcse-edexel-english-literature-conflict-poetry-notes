#import "lib.typ": poem

#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes #h(1fr)
  The Man He Killed
])

#poem("The Man He Killed", "Thomas Hardy", "1902", (
  [‘Had he and I but met],
  [By some old ancient inn,],
  [We should have sat us down to wet],
  [Right many a nipperkin!],
  none,
  [‘But ranged as infantry,],
  [And staring face to face,],
  [I shot at him as he at me,],
  [And killed him in his place.],
  none,
  [‘I shot him dead because –],
  [Because he was my foe,],
  [Just so: my foe of course he was;],
  [That’s clear enough; although],
  none,
  [‘He thought he’d ’list, perhaps,],
  [Off-hand like – just as I –],
  [Was out of work – had sold his traps –],
  [No other reason why.],
  none,
  [‘Yes; quaint and curious war is!],
  [You shoot a fellow down],
  [You’d treat if met where any bar is,],
  [Or help to half-a-crown.’],
))

#pagebreak(weak: true)