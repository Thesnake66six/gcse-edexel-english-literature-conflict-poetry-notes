#import "lib.typ": poem

#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes #h(1fr)
  War Photographer
])


#poem("War Photographer", "Carole Satyamurti", "1987", (
  [The reassurance of the frame is flexible],
  [– you can think that just outside it],
  [people eat, sleep, love normally],
  [while I seek out the tragic, the absurd,],
  [to make a subject.],
  [Or if the picture’s such as lifts the heart],
  [the firmness of the edges can convince you],
  [this is how things are],
  none,
  [– as when at Ascot once],
  [I took a pair of peach, sun-gilded girls],
  [rolling, silk-crumpled, on the grass],
  [in champagne giggles],
  none,
  [– as last week, when I followed a small girl],
  [staggering down some devastated street,],
  [hip thrust out under a baby’s weight.],
  [She saw me seeing her; my finger pressed.],
  none,
  [At the corner, the first bomb of the morning],
  [shattered the stones.],
  [Instinct prevailing, she dropped her burden],
  [and, mouth too small for her dark scream,],
  [began to run…],
  none,
  [The picture showed the little mother],
  [the almost-smile. Their caption read],
  [‘Even in hell the human spirit],
  [triumphs over all.’],
  [But hell, like heaven, is untidy,],
  [its boundaries],
  [arbitrary as a blood stain on a wall.],
))

#pagebreak(weak: true)