#import "lib.typ": poem

#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes #h(1fr)
  The Destruction of Sennacherib
])

#poem("The Destruction of Sennacherib", "Lord Byron", "1815", (
  [The Assyrian came down like the wolf on the fold,],
  [And his cohorts were gleaming in purple and gold;],
  [And the sheen of their spears was like stars on the sea,],
  [When the blue wave rolls nightly on deep Galilee.],
  none,
  [Like the leaves of the forest when Summer is green,],
  [That host with their banners at sunset were seen:],
  [Like the leaves of the forest when Autumn hath blown,],
  [That host on the morrow lay wither’d and strown.],
  none,
  [For the Angel of Death spread his wings on the blast,],
  [And breathed in the face of the foe as he pass’d;],
  [And the eyes of the sleepers wax’d deadly and chill,],
  [And their hearts but once heaved, and for ever grew still!],
  none,
  [And there lay the steed with his nostril all wide,],
  [But through it there roll’d not the breath of his pride:],
  [And the foam of his gasping lay white on the turf,],
  [And cold as the spray of the rock-beating surf.],
  none,
  [And there lay the rider distorted and pale,],
  [With the dew on his brow and the rust on his mail;],
  [And the tents were all silent, the banners alone,],
  [The lances unlifted, the trumpet unblown.],
  none,
  [And the widows of Ashur are loud in their wail,],
  [And the idols are broke in the temple of Baal;],
  [And the might of the Gentile, unsmote by the sword,],
  [Hath melted like snow in the glance of the Lord!],
))

#pagebreak(weak: true)