#import "lib.typ": poem

#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes #h(1fr)
  Belfast Confetti
])


#poem(
  "Belfast Confetti",
  "Ciaran Carson",
  "1990",
  (
    [Suddenly as the riot squad moved in, it was raining exclamation marks,],
    [Nuts, bolts, nails, car-keys. A fount of broken type. And the explosion.],
    [Itself - an asterisk on the map. This hyphenated line, a burst of rapid fire…],
    [I was trying to complete a sentence in my head but it kept stuttering,],
    [All the alleyways and side streets blocked with stops and colons.],
    none,
    [I know this labyrinth so well - Balaclava, Raglan, Inkerman, Odessa Street -],
    [Why can’t I escape? Every move is punctuated. Crimea Street. Dead end again.],
    [A Saracen, Kremlin-2 mesh. Makrolon face-shields. Walkie- talkies. What is],
    [My name? Where am I coming from? Where am I going? A fusillade of question-marks],
  ),
)

#pagebreak(weak: true)