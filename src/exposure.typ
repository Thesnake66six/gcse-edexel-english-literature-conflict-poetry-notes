#import "lib.typ": poem

#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes #h(1fr)
  Exposure
])

#poem(
  "Exposure",
  "Wilfred Owen",
  "1917",
  (
    [Our brains ache, in the merciless iced east winds that knive us…],
    [Wearied we keep awake because the night is silent…],
    [Low, drooping flares confuse our memories of the salient…],
    [Worried by silence, sentries whisper, curious, nervous,],
    [But nothing happens.],
    none,
    [Watching, we hear the mad gusts tugging on the wire,],
    [Like twitching agonies of men among its brambles.],
    [Northward, incessantly, the flickering gunnery rumbles,],
    [Far off, like a dull rumour of some other war.],
    [What are we doing here?],
    none,
    [The poignant misery of dawn begins to grow…],
    [We only know war lasts, rain soaks, and clouds sag stormy.],
    [Dawn massing in the east her melancholy army],
    [Attacks once more in ranks on shivering ranks of grey,],
    [But nothing happens.],
    none,
    [Sudden successive flights of bullets streak the silence.],
    [Less deadly than the air that shudders black with snow,],
    [With sidelong flowing flakes that flock, pause, and renew,],
    [We watch them wandering up and down the wind’s nonchalance],
    [But nothing happens.],
    none,
    [Pale flakes with fingering stealth come feeling for our faces –],
    [We cringe in holes, back on forgotten dreams, and stare, snow-dazed,],
    [Deep into grassier ditches. So we drowse, sun-dozed,],
    [Littered with blossoms trickling where the blackbird fusses.],
    [Is it that we are dying?],
    none,
    [Slowly our ghosts drag home: glimpsing the sunk fires, glozed],
    [With crusted dark-red jewels; crickets jingle there;],
    [For hours the innocent mice rejoice: The house is theirs;],
    [Shutters and doors, all closed: on us the doors are closed, –],
    [We turn back to our dying.],
    none,
    [Since we believe not otherwise can kind fires burn;],
    [Nor ever suns smile true on child, or field, or fruit.],
    [For God’s invincible spring our love is made afraid;],
    [Therefore, not loath, we lie out here; therefore were born,],
    [For love of God seems dying.],
    none,
    [Tonight, His frost will fasten on this mud and us,],
    [Shrivelling many hands, puckering foreheads crisp.],
    [The burying party, picks and shovels in the shaking grasp,],
    [Pause over half-known faces. All their eyes are ice,],
    [But nothing happens.],
  ),
)

#pagebreak(weak: true)