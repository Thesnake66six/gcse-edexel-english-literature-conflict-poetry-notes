#import "lib.typ": poem

#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes #h(1fr)
  No Problem
])


#poem("No Problem", "Benjamin Zephaniah", "1996", (
  [I am not de problem],
  [But I bear de brunt],
  [Of silly playground taunts],
  [An racist stunts,],
  [I am not de problem],
  [I am born academic],
  [But dey got me on de run],
  [Now I am branded athletic],
  [I am not de problem],
  [If yu give I a chance],
  [I can teach yu of Timbuktu],
  [I can do more dan dance,],
  [I am not de problem],
  [I greet yu wid a smile],
  [Yu put me in a pigeon hole],
  [But I am versatile],
  none,
  [These conditions may affect me],
  [As I get older,],
  [An I am positively sure],
  [I have no chips on me shoulders,],
  [Black is not de problem],
  [Mother country get it right],
  [An juss fe de record,],
  [Sum of me best friends are white.],
))

#pagebreak(weak: true)