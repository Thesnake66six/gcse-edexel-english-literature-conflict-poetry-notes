#import "lib.typ": poem

#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes #h(1fr)
  Half-caste
])

#poem("Half-caste", "John Agard", "1996", (
  [Excuse me],
  [standing on one leg],
  [I’m half-caste],
  none,
  [Explain yuself],
  [wha yu mean],
  [when you say half-caste],
  [yu mean when picasso],
  [mix red an green],
  [is a half-caste canvas/],
  [explain yuself],
  [wha yu mean],
  [when yu say half-caste],
  [yu mean when light an shadow],
  [mix in de sky],
  [is a half-caste weather/],
  [well in dat case],
  [england weather],
  [nearly always half-caste],
  [in fact some o dem cloud],
  [half-caste till dem overcast],
  [so spiteful dem dont want de sun pass],
  [ah rass/],
  [explain yuself],
  [wha yu mean],
  [when you say half-caste],
  [yu mean tchaikovsky],
  [sit down at dah piano],
  [an mix a black key],
  [wid a white key],
  [is a half-caste symphony/],
  [Explain yuself],
  [wha yu mean],
  [Ah listening to yu wid de keen],
  [half of mih ear],
  [Ah lookin at yu wid de keen],
  [half of mih eye],
  [and when I’m introduced to yu],
  [I’m sure you’ll understand],
  [why I offer yu half-a-hand],
  [an when I sleep at night],
  [I close half-a-eye],
  [consequently when I dream],
  [I dream half-a-dream],
  [an when moon begin to glow],
  [I half-caste human being],
  [cast half-a-shadow],
  [but yu must come back tomorrow],
  [wid de whole of yu eye],
  [an de whole of yu ear],
  [an de whole of yu mind],
  none,
  [an I will tell yu],
  [de other half],
  [of my story],
))

#pagebreak(weak: true)