#import "lib.typ": poem

#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes #h(1fr)
  Poppies
])

#poem(
  "Poppies",
  "Jane Weir",
  "2005",
  (
    [Three days before Armistice Sunday],
    [and poppies had already been placed],
    [on individual war graves. Before you left,],
    [I pinned one onto your lapel, crimped petals,spasms of paper red, disrupting a
      blockade],
    [of yellow bias binding around your blazer.],
    none,
    [Sellotape bandaged around my hand,],
    [I rounded up as many white cat hairs],
    [as I could, smoothed down your shirt’s],
    [upturned collar, steeled the softening],
    [of my face. I wanted to graze my nose],
    [across the tip of your nose, play at],
    [being Eskimos like we did when],
    [you were little. I resisted the impulse],
    [to run my fingers through the gelled],
    [blackthorns of your hair. All my words],
    [flattened, rolled, turned into felt,],
    none,
    [slowly melting. I was brave, as I walked],
    [with you, to the front door, threw],
    [it open, the world overflowing],
    [like a treasure chest. A split second],
    [and you were away, intoxicated.],
    [After you’d gone I went into your bedroom,],
    [released a song bird from its cage.],
    [Later a single dove flew from the pear tree,],
    [and this is where it has led me,],
    [skirting the church yard walls, my stomach busy],
    [making tucks, darts, pleats, hat-less, without],
    [a winter coat or reinforcements of scarf, gloves.],
    none,
    [On reaching the top of the hill I traced],
    [the inscriptions on the war memorial,],
    [leaned against it like a wishbone.],
    [The dove pulled freely against the sky,],
    [an ornamental stitch. I listened, hoping to hear],
    [your playground voice catching on the wind.],
  ),
)

#pagebreak(weak: true)