#import "lib.typ": poem

#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes #h(1fr)
  Catrin
])


#poem("Catrin", "Gillian Clarke", "1978", (
  [I can remember you, child,],
  [As I stood in a hot, white],
  [Room at the window watching],
  [The people and cars taking],
  [Turn at the traffic lights.],
  [I can remember you, our first],
  [Fierce confrontation, the tight],
  [Red rope of love which we both],
  [Fought over. It was a square],
  [Environmental blank, disinfected],
  [Of paintings or toys. I wrote],
  [All over the walls with my],
  [Words, coloured the clean squares],
  [With the wild, tender circles],
  [Of our struggle to become],
  [Separate. We want, we shouted,],
  [To be two, to be ourselves.],
  none,
  [Neither won nor lost the struggle],
  [In the glass tank clouded with feelings],
  [Which changed us both. Still I am fighting],
  [You off, as you stand there],
  [With your straight, strong, long],
  [Brown hair and your rosy,],
  [Defiant glare, bringing up],
  [From the heart’s pool that old rope,],
  [Tightening about my life,],
  [Trailing love and conflict,],
  [As you ask may you skate],
  [In the dark, for one more hour.],
))

#pagebreak(weak: true)