#import "lib.typ": poem

#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes #h(1fr)
  The Class Game
])


#poem("The Class Game", "Mary Casey", "1981", (
  [How can you tell what class I’m from?],
  [I can talk posh like some],
  [With an ’Olly in me mouth],
  [Down me nose, wear an ’at not a scarf],
  [With me second-hand clothes.],
  [So why do you always wince when you hear],
  [Me say ‘Tara’ to me ‘Ma’ instead of ‘Bye Mummy],
  [dear’?],
  [How can you tell what class I’m from?],
  [’Cos we live in a corpy, not like some],
  [In a pretty little semi, out Wirral way],
  [And commute into Liverpool by train each day?],
  [Or did I drop my unemployment card],
  [Sitting on your patio (We have a yard)?],
  [How can you tell what class I’m from?],
  [Have I a label on me head, and another on me bum?],
  [Or is it because my hands are stained with toil?],
  [Instead of soft lily-white with perfume and oil?],
  [Don’t I crook me little finger when I drink me tea],
  [Say toilet instead of bog when I want to pee?],
  [Why do you care what class I’m from?],
  [Does it stick in your gullet like a sour plum?],
  [Well, mate! A cleaner is me mother],
  [A docker is me brother],
  [Bread pudding is wet nelly],
  [And me stomach is me belly],
  [And I’m proud of the class that I come from.],
))

#pagebreak(weak: true)