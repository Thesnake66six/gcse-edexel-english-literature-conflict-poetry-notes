#let poem(name, poet, date, lines) = {
  let none_lines = 0;
  let lines_new = ()
  for line_with_n in lines.enumerate() {
    let line = line_with_n.at(1)
    if line == none {
      none_lines += 1
    }
    let n = line_with_n.at(0)
    let n = n + 1 - none_lines
    lines_new.push([#{
        if line != none and calc.rem(n, 5) == 0 {
          n
        } else {
          []
        }
      }])
    lines_new.push(align(left + top, par(hanging-indent: 1em, line)))
  }
   
  align(top + center, stack(
    dir: ttb,
    spacing: 1.5em,
    [== #name],
    grid(columns: (auto, auto), gutter: 1em, ..lines_new),
    align(bottom + right, pad(x: 4em, emph([#poet, #date]))),
  ))
}