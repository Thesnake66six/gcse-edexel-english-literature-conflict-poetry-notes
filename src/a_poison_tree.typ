#import "lib.typ": poem

#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes #h(1fr)
  A Poison Tree
])


#poem("A Poison Tree", "William Blake", "1794", (
  [I was angry with my friend:],
  [I told my wrath, my wrath did end.],
  [I was angry with my foe:],
  [I told it not, my wrath did grow.],
  none,
  [And I water’d it in fears,],
  [Night and morning with my tears;],
  [And I sunned it with smiles,],
  [And with soft deceitful wiles.],
  none,
  [And it grew both day and night,],
  [Till it bore an apple bright;],
  [And my foe beheld it shine,],
  [And he knew that it was mine,],
  none,
  [And into my garden stole],
  [When the night had veil’d the pole:],
  [In the morning glad I see],
  [My foe outstretch’d beneath the tree.],
))

#pagebreak(weak: true)