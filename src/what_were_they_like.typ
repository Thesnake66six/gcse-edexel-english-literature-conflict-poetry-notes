#import "lib.typ": poem

#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes #h(1fr)
  What Were They Like?
])


#poem("What Were They Like?", "Denise Levertov", "1967", (
  [1) Did the people of Viet Nam],
  [use lanterns of stone?],
  [2) Did they hold ceremonies],
  [to reverence the opening of buds?],
  [3) Were they inclined to quiet laughter?],
  [4) Did they use bone and ivory,],
  [jade and silver, for ornament?],
  [5) Had they an epic poem?],
  [6) Did they distinguish between speech and singing?],
  none,
  [1) Sir, their light hearts turned to stone.],
  [It is not remembered whether in gardens],
  [stone lanterns illumined pleasant ways.],
  [2)Perhaps they gathered once to delight in blossom,],
  [but after their children were killed],
  [there were no more buds],
  [3) Sir, laughter is bitter to the burned mouth.],
  [4) A dream ago, perhaps. Ornament is for joy.],
  [All the bones were charred.],
  [5) It is not remembered. Remember,],
  [most were peasants; their life],
  [was in rice and bamboo.],
  [When peaceful clouds were reflected in the paddies],
  [and the water buffalo stepped surely along terraces,],
  [maybe fathers told their sons old tales.],
  [When bombs smashed those mirrors],
  [there was time only to scream.],
  [6) There is an echo yet],
  [of their speech which was like a song.],
  [It was reported that their singing resembled],
  [the flight of moths in moonlight.],
  [Who can say? It is silent now.],
))

#pagebreak(weak: true)