#import "lib.typ": poem

#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes #h(1fr)
  #emph[Extract from] The Prelude
])


#poem([#emph[Extract from] The Prelude], "William Wordworth", "1850", (
  [One summer evening (led by her) I found],
  [A little boat tied to a willow tree],
  [Within a rocky cove, its usual home.],
  [Straight I unloosed her chain, and stepping in],
  [Pushed from the shore. It was an act of stealth],
  [And troubled pleasure, nor without the voice],
  [Of mountain-echoes did my boat move on;],
  [Leaving behind her still, on either side,],
  [Small circles glittering idly in the moon,],
  [Until they melted all into one track],
  [Of sparkling light. But now, like one who rows,],
  [Proud of his skill, to reach a chosen point],
  [With an unswerving line, I fixed my view],
  [Upon the summit of a craggy ridge,],
  [The horizon’s utmost boundary; far above],
  [Was nothing but the stars and the grey sky.],
  [She was an elfin pinnace; lustily],
  [I dipped my oars into the silent lake,],
  [And, as I rose upon the stroke, my boat],
  [Went heaving through the water like a swan;],
  [When, from behind that craggy steep till then],
  [The horizon’s bound, a huge peak, black and huge,],
  [As if with voluntary power instinct,],
  [Upreared its head. I struck and struck again,],
  [And growing still in stature the grim shape],
  [Towered up between me and the stars, and still,],
  [For so it seemed, with purpose of its own],
  [And measured motion like a living thing,],
  [Strode after me. With trembling oars I turned,],
  [And through the silent water stole my way],
  [Back to the covert of the willow tree;],
  [There in her mooring-place I left my bark, –],
  [And through the meadows homeward went, in grave],
  [And serious mood; but after I had seen],
  [That spectacle, for many days, my brain],
  [Worked with a dim and undetermined sense],
  [Of unknown modes of being; o’er my thoughts],
  [There hung a darkness, call it solitude],
  [Or blank desertion. No familiar shapes],
  [Remained, no pleasant images of trees,],
  [Of sea or sky, no colours of green fields;],
  [But huge and mighty forms, that do not live],
  [Like living men, moved slowly through the mind],
  [By day, and were a trouble to my dreams.],
))

#pagebreak(weak: true)