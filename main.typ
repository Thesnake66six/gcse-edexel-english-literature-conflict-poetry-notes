#set page(margin: (top: 92pt, bottom: 60pt), header: [
  #set text(11pt, fill: rgb("#777"))
  Poetry Anthology Notes
], footer: [
  #set align(center)
  #set text(11pt)
  #counter(page).display("1")
])

#text(size: 20pt, [*Poetry Anthology Notes*])

#outline(indent: 2em)

#pagebreak(weak: true)

#include "src/a_poison_tree.typ"
#include "src/the_destruction_of_sennacherib.typ"
#include "src/extract_from_the_prelude.typ"
#include "src/the_man_he_killed.typ"
#include "src/cousin_kate.typ"
#include "src/half-caste.typ"
#include "src/exposure.typ"
#include "src/catrin.typ"
#include "src/war_photographer.typ"
#include "src/belfast_confetti.typ"
#include "src/the_class_game.typ"
#include "src/poppies.typ"
#include "src/no_problem.typ"
#include "src/what_were_they_like.typ"
